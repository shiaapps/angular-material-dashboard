'use strict';

var gulp = require('gulp');
var clean = require('gulp-clean');

gulp.paths = {
  src: 'src',
  dist: 'dist',
  tmp: '.tmp',
  e2e: 'e2e'
};

require('require-dir')('./gulp');

gulp.task('default', ['clean'], function () {
    return gulp.start('build');
});