(function() {
	'use strict';

	function ResultsService($firebaseArray) {
		var cachedData = [];
		var cachedSchema;
		var schemaCallbacks = {
			'callback' : function() {},
			'errCallback' : function() {}
		};
		var responsesCallbacks = [];

		var filterByDate = function(begin, end) {
			return function(i) {
				var submissionDate = new Date(i.submittedOn);
				var fromDate = new Date(begin);
				var toDate = new Date(end);
				return (submissionDate >= fromDate) && (submissionDate <= toDate);
			};
		};

		return {
			loadSchema: function(callback, errCallback) {
				var ref = firebase.database().ref().child('schema3');
				var schema = $firebaseArray(ref);

				schemaCallbacks.callback = callback;
				schemaCallbacks.errCallback = errCallback;

				schema.$loaded()
					  .then(function(data) {
						  cachedSchema = data;
						  schemaCallbacks.callback(data);
					  })
					  .then(function() {
						  // disabled the real-time schema update because the charts do not support it
						//   schema.$watch(function() {
						// 	  callback(schema);
						//   });
					  })
					  .catch(schemaCallbacks.errCallback);
			},
			loadResponses: function(scopeCallback, callback, errCallback) {
				var ref = firebase.database().ref().child('responses');
				var responses = $firebaseArray(ref);

				var callbacks = [];
				callbacks.scopeCallback = scopeCallback;
				callbacks.callback = callback;
				callbacks.errCallback = errCallback;

				responsesCallbacks.push(callbacks);

				responses.$loaded()
						  .then(function(data) {
							  var scope = responsesCallbacks[1].scopeCallback();
							  cachedData = data;
							  responsesCallbacks.forEach(function(callbackObj) {
								  callbackObj.callback(cachedData.filter(filterByDate(scope.fromDate, scope.toDate)));
							  });
						  })
						  .then(function() {
							  responses.$watch(function() {
								  var scope = responsesCallbacks[1].scopeCallback();
								  cachedData = responses;
								responsesCallbacks.forEach(function(callbackObj) {
									callbackObj.callback(cachedData.filter(filterByDate(scope.fromDate, scope.toDate)));
								});
							  });
						  })
						  .catch(function(error) {
							  responsesCallbacks.forEach(function(callbackObj) {
								  callbackObj.errCallback(error);
							  });
						  });
			},
			applyPeriodFilter: function(fromDate, toDate) {
				if (cachedData !== undefined && responsesCallbacks.length >= 0) {
					responsesCallbacks.forEach(function(callbackObj) {
						callbackObj.callback(cachedData.filter(filterByDate(fromDate, toDate)));
					});
				}
			},
			prepareDownloadFile: function(callback) {
				responsesCallbacks
					.push(
						{
							'callback' : function(data) {

								if (cachedSchema === undefined) {
									return;
								}

								const replacer = function(key, value) {
									return value === null ? '' : value; // specify how you want to handle null values here
								}

								// queestion titles as associated array
								const questionTitles = cachedSchema
														.map(function(section) {
															return Object.keys(section.questions)
																			.map(function(i) {
																				return { 
																					'id' : section.questions[i].id, 
																					'title' : section.questions[i].title 
																				};
																			});
														})
														.reduce(function(acc, curr) {
															return [].concat.apply(acc, curr);
														}, [])
														.reduce(function(acc, curr) {
															acc[curr.id] = curr.title;
															return acc;
														}, []);

								// this one controls the order and which fields to output
								var headerKeys = Object.keys(questionTitles);
								headerKeys.unshift('$id');
								headerKeys.push('submittedOn');

								const headerRow = headerKeys
													.map(function(fieldKey) {
														if (fieldKey === '$id' || fieldKey === 'submittedOn') {
															return fieldKey;
														} else {
															return questionTitles[fieldKey];
														}
													});
								var csv = data
											.map(function(row) {
												return headerKeys
													.map(function(fieldName) {
														return JSON.stringify(row[fieldName], replacer);
													})
													.join(',');
												}
											);

								csv.unshift(headerRow.join(','));
								csv = csv.join('\r\n');

								var blob = new Blob([ csv ], { type : 'text/csv' });
								callback(blob);
							}
						}
					);
			}
		};
	}

	ResultsService.$inject = ['$firebaseArray'];

    angular.module('app')
		   .factory('ResultsService', ResultsService);
})();
