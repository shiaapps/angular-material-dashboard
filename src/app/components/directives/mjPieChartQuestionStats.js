(function() {
    'use strict';

    function PieChartQuestionStatsController($scope, ResultsService) {

        var vm = this;

        var statsForQuestionInResults = function(questionId, results) {
            var possibleAnswers = Object.keys(vm.labels);
            var stats = [];
            var getIdFunc = function(i) { return i[questionId]; };
            var countAnswersMatching = function(a) {
                return function(count, answer) {
                    return (answer === a) ? count+1 : count;
                };
            };

            for (var a in possibleAnswers) {
                var count = results.map(getIdFunc)
                                   .reduce(countAnswersMatching(a), 0);
                stats.push( { key: a, y: count } );
            }
            return stats;
        };

        var getChartOptions = function() {
            return {
                chart: {
                    type: 'pieChart',
                    donut: true,
                    x: function (d) { return vm.labels[d.key]; },
                    y: function (d) { return d.y; },
                    valueFormat: (d3.format('.0f')),
                    showLabels: true,
                    labelType: 'percent',
                    'legend': {
                        'padding' : 36,
                        'rightAlign' : true,
                        'margin' : { top: 8 , right: -8 }
                    },
                    'donutRatio': 0.35,
                    margin: { top: 30 , bottom: -16 },
                    noData: 'جاري التحديث'
                },
                'title': {
                    'enable': true,
                    'text': vm.title
                }
            };
        };

        vm.chartData = [];

        ResultsService.loadResponses(function() {
            return vm;
        }, function(data) {
            vm.chartData = statsForQuestionInResults(vm.questionId, data);
            vm.chartOptions = getChartOptions();
        }, function(error) {
            console.error(error);
        } );

        vm.chartOptions = getChartOptions();

        vm.chartConfig = {
            'refreshDataOnly' : true
        };
    }

    PieChartQuestionStatsController.$inject = ['$scope', 'ResultsService'];

    function mjPieChartQuestionStats() {
        return {
            restrict: 'E',
            replace: true,
            scope: { title: '@' },
            controller: 'PieChartQuestionStatsController as vm',
            template: '' +
                      '<section flex layout-margin class="md-whiteframe-z1 panel-widget" style="height: 280px;" dir="ltr">' +
                      '  <md-content layout="row" layout-align="center center" style="background: white; height: 100%; top: -16px; position: relative;">' +
                      '    <nvd3 options="vm.chartOptions" data="vm.chartData" config="vm.chartConfig" style="width: 100%; height: 84%"></nvd3>' +
                      '  </md-content>' +
                      '</section>',
            link : function(scope, element, attrs) {
                scope.vm.title = attrs.title;
                scope.vm.questionId = attrs.questionId;
                scope.vm.questionType = attrs.questionType;
                scope.vm.labels = JSON.parse(attrs.labels);
                scope.vm.fromDate = JSON.parse(attrs.filterFrom);
                scope.vm.toDate = JSON.parse(attrs.filterTo);
            }
        };
    }


    angular.module('app')
        .directive('mjPieChartQuestionStats', mjPieChartQuestionStats);

    angular.module('app')
        .controller('PieChartQuestionStatsController', PieChartQuestionStatsController);
})();
