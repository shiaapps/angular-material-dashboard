(function() {
    'use strict';

    function BarChartQuestionStatsController($scope, ResultsService) {

        var vm = this;

        var statsForQuestionInResults = function(questionId, results) {
            var possibleAnswers = Object.keys(vm.labels);
            var stats = [];
            var getIdFunc = function(i) { return i[questionId]; };
            var countAnswersMatching = function(a) {
                return function(count, answer) {
                    return (answer === a) ? count+1 : count;
                };
            };

            for (var a in possibleAnswers) {
                var count = results.map(getIdFunc)
                                   .reduce(countAnswersMatching(a), 0);
                stats.push( { label: vm.labels[a], value: count } );
            }

            return [
                {
                    key: 'BarChart',
                    values: stats
                }
            ];
        };

        var getChartOptions = function() {
            return {
                chart: {
                    type: 'discreteBarChart',
                    x: function (d) { return d.label; },
                    y: function (d) { return d.value; },
                    showValues: true,
                    'tooltip': {
                        'enabled': false
                    },
                    valueFormat: (d3.format('.0f')),
                    noData: 'جاري التحديث'
                },
                'title': {
                    'enable': true,
                    'text': vm.title
                }
            };
        };

        vm.chartData = [
            {
                key: 'BarChart',
                values: [
                    {label: 'd', value: 0 },
                    {label: 'w', value: 0 }
                ]
            }
        ];

        ResultsService.loadResponses(function() {
            return vm;
        }, function(data) {
            vm.chartData = statsForQuestionInResults(vm.questionId, data);
            vm.chartOptions = getChartOptions();
        }, function(error) {
            console.error(error);
        });

        vm.chartOptions = getChartOptions();

        vm.chartOptions = {
            chart: {
                height: '280',
                type: 'discreteBarChart',
                x: function (d) { return d.label; },
                y: function (d) { return d.value; },
                showValues: true,
                valueFormat: (d3.format('.0f')),
                margin: { top: 30 , bottom: -16 },
                noData: 'جاري التحديث'
            },
            'title': {
                'enable': true,
                'text': vm.title
            }
        };

        vm.chartConfig = {
            'refreshDataOnly' : true
        };
    }

    BarChartQuestionStatsController.$inject = ['$scope', 'ResultsService'];

    function mjBarChartQuestionStats() {
        return {
            restrict: 'E',
            replace: true,
            scope: { title: '@' },
            controller: 'BarChartQuestionStatsController as vm',
            template: '' +
                      '<section flex layout-margin class="md-whiteframe-z1 panel-widget" style="height: 280px;" dir="ltr">' +
                      '  <md-content layout="row" layout-align="center center" style="background: white; height: 100%; top: -16px; position: relative;">' +
                      '    <nvd3 options="vm.chartOptions" data="vm.chartData" config="vm.chartConfig" style="width: 100%; height: 84%"></nvd3>' +
                      '  </md-content>' +
                      '</section>',
            link : function(scope, element, attrs) {
                scope.vm.title = attrs.title;
                scope.vm.questionId = attrs.questionId;
                scope.vm.questionType = attrs.questionType;
                scope.vm.labels = JSON.parse(attrs.labels);
                scope.vm.fromDate = JSON.parse(attrs.filterFrom);
                scope.vm.toDate = JSON.parse(attrs.filterTo);
            }
        };
    }


    angular.module('app')
        .directive('mjBarChartQuestionStats', mjBarChartQuestionStats);

    angular.module('app')
        .controller('BarChartQuestionStatsController', BarChartQuestionStatsController);
})();
