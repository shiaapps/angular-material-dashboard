(function() {
    'use strict';

    function TableQuestionStatsController($scope, ResultsService) {

        var vm = this;

        var answersForQuestion = function(questionId, results) {
            var answers = [];
            answers = results.map(function(i) {
                                 return i[questionId];
                             })
                             .filter(function(i) {
                                 return i !== '';
                             });
            return answers;
        };

        ResultsService.loadResponses(function() {
            return vm;
        }, function(data) {
            vm.answers = answersForQuestion(vm.questionId, data);
        }, function(error) {
            console.error(error);
        });
    }

    TableQuestionStatsController.$inject = ['$scope', 'ResultsService'];

    function mjTableQuestionStats() {
        return {
            restrict: 'E',
            replace: true,
            scope: { title: '@' },
            controller: 'TableQuestionStatsController as vm',
            template: '' +
                      '<section flex layout-margin class="md-whiteframe-z1 panel-widget" style="height: 475px">' +
                      '  <md-toolbar md-theme="dark" class="md-hue-3 panel-widget-toolbar">' +
                      '    <div class="md-toolbar-tools">' +
                      '      <h3 class="panel-widget-tittle">{{title}}</h3>' +
                      '      <span flex></span>' +
                      '      <md-button ng-show="options" ng-click="$showOptions = !$showOptions" class="md-icon-button" aria-label="Show options">' +
                      '        <i class="material-icons">more_vert</i>' +
                      '      </md-button>' +
                      '    </div>' +
                      '  </md-toolbar>' +
                      '  <md-content layout="row" layout-align="center center" style="background: white; height: 375px">' +
                      '    <table id="table" class="table table-hover table-bordered" style="position: absolute; top: 0">' +
                      '        <thead>' +
                      '        <tr>' +
                      '            <th>#</th>' +
                      '            <th>الإجابة</th>' +
                      '        </tr>' +
                      '        </thead>' +
                      '        <tbody>' +
                      '        <tr ng-repeat="answer in vm.answers track by $index">' +
                      '            <td data-title="ID" width="1px">{{$index + 1}}</td>' +
                      '            <td data-title="Title">{{answer}}</td>' +
                      '        </tr>' +
                      '        </tbody>' +
                      '    </table>' +
                      '  </md-content>' +
                      '</section>',
            link : function(scope, element, attrs) {
                scope.vm.title = attrs.title;
                scope.vm.questionId = attrs.questionId;
                scope.vm.questionType = attrs.questionType;
                scope.vm.fromDate = JSON.parse(attrs.filterFrom);
                scope.vm.toDate = JSON.parse(attrs.filterTo);
            }
        };
    }


    angular.module('app')
        .directive('mjTableQuestionStats', mjTableQuestionStats);

    angular.module('app')
        .controller('TableQuestionStatsController', TableQuestionStatsController);
})();
