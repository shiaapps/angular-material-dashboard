(function(){
    'use strict';


    function MainController(navService, $mdSidenav, $mdBottomSheet, $log, $q, $state, $mdToast, ResultsService) {
        var vm = this;

        vm.menuItems = [ ];
        vm.selectItem = selectItem;
        vm.toggleItemsList = toggleItemsList;
        vm.showActions = showActions;
        vm.title = $state.current.data.title;
        vm.showSimpleToast = showSimpleToast;
        vm.toggleRightSidebar = toggleRightSidebar;
        vm.downloadReady = false;

        ResultsService
            .prepareDownloadFile(function(blob) {
                var urlSvc = (window.URL || window.webkitURL);
                // revoke previous object
                if (vm.url !== undefined) {
                    urlSvc.revokeObjectURL(vm.url);
                }

                // var blob = new Blob([ content ], { type : 'text/plain' });
                vm.url = urlSvc.createObjectURL( blob );
                vm.downloadReady = true;
            });

        navService
            .loadAllItems()
            .then(function(menuItems) {
                vm.menuItems = [].concat(menuItems);
            });

        function toggleRightSidebar() {
            $mdSidenav('right').toggle();
        }

        function toggleItemsList() {
          var pending = $mdBottomSheet.hide() || $q.when(true);

          pending.then(function(){
            $mdSidenav('left').toggle();
          });
        }

        function selectItem (item) {
          vm.title = item.name;
          vm.toggleItemsList();
          vm.showSimpleToast(vm.title);
        }

        function showActions($event) {
            $mdBottomSheet.show({
              parent: angular.element(document.getElementById('content')),
              templateUrl: 'app/views/partials/bottomSheet.html',
              controller: [ '$mdBottomSheet', SheetController],
              controllerAs: "vm",
              bindToController : true,
              targetEvent: $event
            }).then(function(clickedItem) {
              clickedItem && $log.debug( clickedItem.name + ' clicked!');
            });

            function SheetController( $mdBottomSheet ) {
              var vm = this;

              vm.actions = [
                { name: 'Share', icon: 'share', url: 'https://twitter.com/intent/tweet?text=Angular%20Material%20Dashboard%20https://github.com/flatlogic/angular-material-dashboard%20via%20@flatlogicinc' },
                { name: 'Star', icon: 'star', url: 'https://github.com/flatlogic/angular-material-dashboard/stargazers' }
              ];

              vm.performAction = function(action) {
                $mdBottomSheet.hide(action);
              };
            }
        }

        function showSimpleToast(title) {
          $mdToast.show(
            $mdToast.simple()
              .content(title)
              .hideDelay(2000)
              .position('bottom right')
          );
        }
    }

    MainController.$inject = ['navService', '$mdSidenav', '$mdBottomSheet', '$log', '$q', '$state', '$mdToast', 'ResultsService'];
    angular
    .module('app')
    .controller('MainController', MainController);
})();
