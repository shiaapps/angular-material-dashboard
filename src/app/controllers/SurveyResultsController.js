(function () {

    angular
        .module('app')
        .controller('SurveyResultsController', SurveyResultsController);

    SurveyResultsController.$inject = ["$scope", "$firebaseObject", "$firebaseArray", "$firebaseAuth", "FBSurveyResults"];

    function SurveyResultsController($scope, $firebaseObject, $firebaseArray, $firebaseAuth, FBSurveyResults) {

        var vm = this;

        // console.log("vm=", vm.questionId);
        // console.log("scope=", $scope.questionId);

        // TODO: move data to the service
        vm.chartData = [{key:1, y:0},{key:1, y:0},{key:1, y:0},{key:1, y:0},{key:1, y:1}];

        // TODO: get the question id as a parameter
        var targetQuestionId = "01";

        var possibleAnswers = ['0', '1', '2', '3', '4'];

        var ref = firebase.database().ref().child('responses');
        // download the data into a local object
        // var responses = $firebaseArray(ref);

        var results = new FBSurveyResults(ref);
        results.$loaded()
            .then(function() {
                vm.chartData = results.statsForQuestion(vm.questionId);
            })
            .then(function() {
                results.$watch(function() {
                    vm.chartData = results.statsForQuestion(vm.questionId);
                    console.log("data changed");
                });
            })
            .catch(function(error) {
                console.log("Error: ", error);
            });



        // responses.$bindTo($scope, "vm")
        //     .then(function() {
        //         console.log("$bindTo");
        //         console.log(vm.chartData);
        //         console.log(responses);
        //     })

        // responses.$loaded()
        //     .then(function(arr) {
        //         //console.log(JSON.stringify(arr));
        //
        //         // TODO: convert the array of responses to summarized result
        //         //        (i.e. group by question and by answers)
        //
        //         var targetQ = "01";
        //
        //         var data = [];
        //
        //         for (var a in possibleAnswers) {
        //
        //             var mapFunc = function(i) { return i[targetQ]; };
        //             var reduceFunc = function(count, answer) {
        //                 return (answer == a) ? count+1 : count;
        //             };
        //
        //             var count = arr.map(mapFunc)
        //                             .reduce(reduceFunc, 0);
        //             var item =
        //             {
        //                 key : a,
        //                 y    : count
        //             };
        //
        //             data.push(item);
        //         }
        //         console.log(data);
        //         vm.chartData = data;
        //     })
        //     .catch(function(error) {
        //         console.log("Error:", error);
        //     });

        vm.chartOptions = {
            chart: {
                type: 'pieChart',
                height: 228,
                donut: true,
                duration: 500,
                x: function (d) { return ratings[d.key]; },
                y: function (d) { return d.y; },
                valueFormat: (d3.format(".0f")),
                showLabels: false,
                showLegend: false,
                margin: { top: -10 , bottom: -10}
            }
        };
    }

    var ratings =
    [
        "سيء",
        "لا بأس",
        "متوسط",
        "جيد جداً",
        "ممتاز"
    ]
})();
