(function () {
	'use strict';

	function DashboardController($scope, ResultsService) {

		var vm = this;
		var today = moment();

		vm.fromDate = today.clone().startOf('day').subtract(1, 'month').toDate();
		vm.toDate = today.clone().endOf('day').toDate();

		vm.periods = [
			{ label: 'يوم أمس',	 	interval: 'day' 	},
			{ label: 'منذ أسبوع',	interval: 'week' 	},
			{ label: 'منذ شهر',	 	interval: 'month' 	},
			{ label: 'منذ عام',		interval: 'year'	},
			{ label: 'منذ البداية',	 interval: '2000'	 },
			{ label: 'آخرى',		interval: ''		}
		];
		vm.interval = 'month';

		var breakIntoSections = function(data) {

			var allQuestions = data
								.map(function(section) {
									return Object.keys(section.questions)
													.map(function(id) {
														return section.questions[id];
													});
								})
								.reduce(function(acc, item) {
									return acc.concat(item);
								}, []);

			var isOfType = function(type) {
				return function(question) {
					return question.type === type;
				};
			}

			return [
				{ questions	: allQuestions.filter(isOfType('Rating'))	},
				{ questions	: allQuestions.filter(isOfType('List'))		},
				{ questions	: allQuestions.filter(isOfType('Text'))		}
			];
		};

		$scope.$watch('vm.fromDate', function(newValue, oldValue, scope) {
			if (newValue !== oldValue) {
				ResultsService.applyPeriodFilter(newValue, scope.vm.toDate);
			}
        });

		$scope.$watch('vm.toDate', function(newValue, oldValue, scope) {
			if (newValue !== oldValue) {
				ResultsService.applyPeriodFilter(scope.vm.fromDate, newValue);
			}
        });

		$scope.$watch('vm.interval', function(newValue, oldValue) {
			if (newValue !== oldValue) {
				if (newValue === '') {
					return;
				}

				var today = moment();
				vm.toDate = today.clone().endOf('day').subtract(1, 'day').toDate();

				if (newValue.startsWith(2)) {
					vm.fromDate = today.startOf('day').year(newValue).toDate();
				} else {
					vm.fromDate = today.startOf('day').subtract(1, newValue).toDate();
				}
			}
        });

		ResultsService.loadSchema(function(data) {
			vm.rows = breakIntoSections(data);
		}, function(error) {
			console.error(error);
		});

	}

	DashboardController.$inject = ['$scope', 'ResultsService'];

    angular
        .module('app')
        .controller('DashboardController', DashboardController);
})();
